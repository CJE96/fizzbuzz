﻿using System;

namespace FizzBuzz
{
    class Program
    {
        static void Main(string[] args)
        {
            bool exitApp = false;

            while (!exitApp)
            {
                Console.WriteLine("Enter an integer between 10 and 1000 to begin. Enter any other key to exit.");

                int number;
                string input = Console.ReadLine();
                bool isInteger = int.TryParse(input, out number);
                if (!isInteger || (isInteger && (number > 1000 || number < 10)))
                {
                    exitApp = true;
                    break;
                }

                Console.WriteLine("Beginning now...");

                int i;
                for (i = 1; i <= number; i++)
                {
                    string text = Convert.ToString(i);

                    if (i % 15 == 0)
                    {
                        text = "FizzBuzz";
                    }
                    else if (i % 5 == 0)
                    {
                        text = "Buzz";
                    }
                    else if (i % 3 == 0)
                    {
                        text = "Fizz";
                    }

                    Console.WriteLine(text);
                }
            }
        }
    }
}
